import React, {
    Component, 
    Fragment
} from "react";

import PropTypes from "prop-types";

import { 
    withTranslation
} from "react-i18next";

import Button from "@material-ui/core/Button";

import _ from "lodash";
import moment from "moment";

import AlertService from "../../modules/alerts/services/AlertService";

import { 
    replaceWithParamsSync
} from "../../modules/utils/string"; 

import "../styles/books.scss";

class BookRow extends Component {
    static propTypes = {
        data: PropTypes.any.isRequired, 
        opEmitter: PropTypes.any
    };

    static defaultProps = {
        ref: () => { }, 
        data: null, 
        opEmitter: null
    };

    isOpEmitterValid = () => {
        const { opEmitter } = this.props;
        return _.isNil(opEmitter) !== true;
    };

    triggerOpEmitter = operation => {
        const { opEmitter, data } = this.props;
        opEmitter.next({op: operation, data: data});
    };

    handleRequestDetails = () => {
        if(!this.isOpEmitterValid()) return ;        
        this.triggerOpEmitter("details");
    };

    handleRequestRemove = () => {
        if(!this.isOpEmitterValid()) return ;        
        this.openAlert();
    };

    handleRequestEdit = () => {
        if(!this.isOpEmitterValid()) return ;        
        this.triggerOpEmitter("edit");
    };

    openAlert = () => {
        const { t, data } = this.props;
        const { name } = data;

        const alertOptions = AlertService.getAlertOptions(
            replaceWithParamsSync(t("labels.remove_customer_alert_title"), { name: name } ), 
            t("labels.confirm_btn_text_alert"), 
            t("labels.cancel_btn_text_alert")
        );

        const onNext = () => this.triggerOpEmitter("remove");

        const onError = err => { };        

        AlertService.openAlertToConfirmRemove(alertOptions).subscribe(onNext, onError);
    };

    getISBN = industryIdentifiers => {
        if(_.isNil(industryIdentifiers)) {
            return null; 
        }

        const isbn = industryIdentifiers.find(item => item["type"] === "ISBN_13")["identifier"];
        return isbn;
    };
    
    render() {
        const { data, t } = this.props;
        const { volumeInfo } = data;
        const { title, subtitle, authors, publisher, publisherDate, industryIdentifiers } = volumeInfo;

        return(
            <Fragment>
                <tr>
                    <td data-label={ t("labels.book") } className="td-text-align-left">
                        <div className="title">{ title }</div>
                        <div className="subtitle">{ subtitle }</div>
                        <div className="subtitle">ISBN: { this.getISBN(industryIdentifiers) }</div>
                    </td>
                    <td data-label={ t("labels.authors") } className="td-text-align-left">
                        <span>{ authors.join(", ") }</span>
                    </td>
                    <td data-label={ t("labels.publishedBy") } className="td-text-align-left">
                        { publisher }
                    </td>
                    <td data-label={ t("labels.publishedAt") } className="td-text-align-right">
                        { moment(publisherDate).format("YYYY") }
                    </td>
                    <td data-labels={ t("labels.actions") }>
                        <Button variant="contained" 
                                color="primary" 
                                onClick={ () => this.handleRequestDetails() }>
                            { t("labels.btn_details") }
                        </Button>
                    </td>
                </tr>
            </Fragment>
        );
    };
};

export default withTranslation(["default"])(BookRow);