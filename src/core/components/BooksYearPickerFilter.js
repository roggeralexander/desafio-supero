import React, {
    Component, 
    Fragment
} from "react";

import PropTypes from "prop-types";

import { 
    withTranslation
} from "react-i18next";

import { 
    DatePicker
} from "@material-ui/pickers";

import CalendarIcon from "@material-ui/icons/CalendarTodayRounded";

import _ from "lodash";

import moment from "moment";

const MIN_DATE = new Date("1900-01-01");
const NOW = moment().toDate();

class BooksYearPickerFilter extends Component {

    static propTypes = {
        resetEM: PropTypes.any, 
        onChange: PropTypes.func
    };

    static defaultProps = {
        ref: () => {}, 
        resetEM: null
    };

    constructor(props) {
        super(props);

        this.state = {
            startDatePicker: null, 
            endDatePicker: null 
        };
    };

    componentDidMount() {
        this.initializeMinMaxDates();
        this.subscribeToResetEM();
    };

    subscribeToResetEM = () => {
        const { resetEM } = this.props;

        if(_.isNil(resetEM)) {
            return ;
        }

        resetEM.subscribe(() => this.initializeMinMaxDates());
    };

    initializeMinMaxDates = () => {
        this.minDateStartDatePicker = MIN_DATE; 
        this.maxDateStartDatePicker = NOW; 
        this.minDateEndDatePicker = this.minDateStartDatePicker; 
        this.maxDateEndDatePicker = this.maxDateStartDatePicker;

        this.setState({
            startDatePicker: null, 
            endDatePicker: null
        });
    };

    handleYearChange = ($moment, type) => {
        let newState = {};

        this.updateMinMaxDates($moment, type);

        newState[`${ type }DatePicker`] = _.isNil($moment) ? null : $moment.toDate();
        const year = _.isNil($moment) ? null : parseInt($moment.format("YYYY"))

        this.setState(newState, () => this.props.onChange(type, year));
    };

    updateMinMaxDates = ($moment, type) => {
        switch(type) {
            case "start": 
                return this.updateMinMaxDateForEndDatePicker($moment);
            default: 
                return this.updateMinMaxDateForStartDatePicker($moment);
        }
    };

    updateMinMaxDateForStartDatePicker = $moment => {
        this.minDateStartDatePicker = MIN_DATE; 
        this.maxDateStartDatePicker = _.isNil($moment) ? NOW : $moment.toDate();
    };

    updateMinMaxDateForEndDatePicker = $moment => {
        this.minDateEndDatePicker = _.isNil($moment) ? MIN_DATE : $moment.toDate();
        this.maxDateEndDatePicker = NOW;
    };

    render() {
        const { t } = this.props;
        const { startDatePicker, endDatePicker } = this.state;

        return (
            <Fragment>
                <span>
                    <span style={{ margin: "0px 10px"}}>
                        <DatePicker views={["year"]} 
                            autoOk
                            clearable 
                            variant="dialog"
                            value={ startDatePicker }  
                            cancelLabel={ t("labels.cancel_btn_text") }
                            clearLabel={ t("labels.reset_btn_text") }
                            minDate= { this.minDateStartDatePicker } 
                            maxDate={ this.maxDateStartDatePicker }
                            onChange={ $event => this.handleYearChange($event, 'start') } />
                        <CalendarIcon />
                    </span>


                    { t("labels.until") }

                    <span style={{ margin: "0px 15px"}}>
                        <DatePicker views={["year"]} 
                            autoOk
                            clearable 
                            variant="dialog"
                            value={ endDatePicker }  
                            cancelLabel={ t("labels.cancel_btn_text") }
                            clearLabel={ t("labels.reset_btn_text") }
                            minDate={ this.minDateEndDatePicker }
                            maxDate={ this.maxDateEndDatePicker }
                            onChange={ $event => this.handleYearChange($event, 'end') } />
                        <CalendarIcon />
                    </span>                            
                </span>
            </Fragment>
        );
    }
};

export default withTranslation(["default"])(BooksYearPickerFilter);
