import React, {
    Component, 
    Fragment 
} from "react";

import PropTypes from "prop-types";

import { 
    withRouter
} from "react-router-dom";

import { 
    withTranslation
} from "react-i18next";

import _ from "lodash";

import { 
    of, 
    Subject
} from "rxjs";

import { 
    switchMap
} from "rxjs/operators";

import BookRow from "../components/BookRow";

import "../styles/books.scss";

class BooksTable extends Component {

    static propTypes = {
        books: PropTypes.array
    };

    static defaultProps = {
        books: []
    };

    constructor(props) {
        super(props);
        this.opEmitter = new Subject();
    };

    componentDidMount() {
        this.subscribeToOpEmitter();
    };

    componentWillUnmount() {
        this.opEmitter.complete();
        this.opEmitter = null; 
    };

    subscribeToOpEmitter = () => {
        const onNext = () => {
            console.log("Operação aplicada com sucesso");
        };

        this.opEmitter.pipe(
            switchMap(obj => this.applyOperationRequestedByOpEmitter(obj))
        ).subscribe(onNext);
    };

    applyOperationRequestedByOpEmitter = obj => {
        const { op, data } = obj; 
        const { id } = data;
        let strUrl = null;

        switch(op) {
            case "details": 
                strUrl = `/book/${ id }`;
                return this.goTo(strUrl, data);
            case "edit": 
                strUrl = `/book/${ id }/edit`;
                return this.goTo(strUrl, data);
            case "remove": 
                return this.removeBook(data);
        }
    };

    goTo = (strUrl, data) => {
        const { history } = this.props;

        if(_.isNil(data) || !Object.keys(data).length) {
            history.push(strUrl);
        } else {
            history.push(strUrl, {data: data});
        }

       return of(undefined);
    };

    removeBook = data => {
        const { id } = data;

        console.log("Remove book: ", id);
    };

    getBooksRowList = () => {
        const { t, books } = this.props;

        if(_.isNil(books) || !books.length) {
            return null;
        }

        return (
            <Fragment>
                <div className="div-table-container">
                    <table>
                        <thead>
                            <tr>
                                <th scope="col" className="td-text-align-left">
                                    { t("labels.book") }
                                </th>
                                <th scope="col" className="td-text-align-left">
                                    { t("labels.authors") }
                                </th>
                                <th scope="col" className="td-text-align-left">
                                    { t("labels.publishedBy") }
                                </th>
                                <th scope="col" className="td-text-align-right">
                                    { t("labels.publishedAt") }
                                </th>
                                <th scope="col">
                                    { t("labels.actions") }
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            { 
                                books.map(obj => (
                                    <BookRow key = {`book-${ obj["id"] }`} 
                                                    data = { obj } 
                                                    opEmitter = { this.opEmitter } />
                                ))
                            }
                        </tbody>
                    </table>
                </div>

            </Fragment>
        );
    };  

    render() {
        return(
            <Fragment>
                { this.getBooksRowList() }
            </Fragment>
        );
    };
};

export default withRouter(
    withTranslation("default")(BooksTable)
);