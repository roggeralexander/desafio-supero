import React, {
    Component, 
    Fragment 
} from "react";

import PropTypes from "prop-types";

import { 
    withTranslation
} from "react-i18next";

import { 
    Subject
} from "rxjs";

import _ from "lodash";

import Autocomplete from "../../modules/autocomplete";

import BooksYearPickerFilter from "./BooksYearPickerFilter";

import BookStoreService from "../services/BookStoreService";

import "../styles/books.scss";

class BooksFilter extends Component {
    static propTypes = {
        requestSearchEM: PropTypes.any.isRequired, 
        selectedIdEM: PropTypes.any.isRequired, 
        totalItems: PropTypes.number
    };

    static defaultProps = {
        ref: () => {}, 
        totalItems: 0, 
    };

    constructor(props) {
        super(props);

        this.resetEM = new Subject();

        this.state = {
            query: "",
            startYear: null, 
            endYear: null, 
            actionsAllowed: false
        };
    };

    componentDidMount() {
        this.subscribeToResetEM();
    };

    componentWillUnmount() {
        this.resetEM.complete(); 
        this.resetEM = null; 
    };

    subscribeToResetEM = () => this.resetEM.subscribe(() => this.resetState());

    checkToAllowActions = () => {
        const { query, startYear, endYear } = this.state;
        let actionsAllowed = false;

        if(!(_.isNil(query) || query.trim().length === 0) || !_.isNil(startYear) || !_.isNil(endYear)) {
            actionsAllowed = true;
        }

        this.setState({actionsAllowed : actionsAllowed });
    };

    handleYearChange = (type, year) => {
        let newState = {};
        newState[`${ type }Year`] = year;
        this.setState(newState, () => this.checkToAllowActions());
    };

    handleResetFilter = () => this.resetEM.next();

    handleAutocompleteReset = () => this.setState({ query: "" }, this.checkToAllowActions());

    handleApplyFilter = () => {
        const { query, startYear, endYear } = this.state;
        // this.props.onFilterChange({ query, startYear, endYear });
    };

    resetState = () => this.setState({
        query: "",
        startYear: null, 
        endYear: null, 
        actionsAllowed: false
    }, () => this.handleApplyFilter());

    render() {
        const { t, totalItems, requestSearchEM, selectedIdEM } = this.props;
        const { actionsAllowed } = this.state;

        return(
            <Fragment>
                <div className="div-block-filter">
                    <div className="div-block-filter-row">
                        <div className="company-logo"></div>

                        <div className="div-filter-autocomplete">
                            <Autocomplete requestSearchEM={ requestSearchEM } 
                                resetEM={ this.resetEM } 
                                selectedIdEM={ selectedIdEM }
                                placeholder={ t("labels.search_books_by") } 
                                cb={ BookStoreService.getSuggestions } 
                                onClear={ () => this.handleAutocompleteReset() } />
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    };

 /*
                    <div className="div-block-filter-row">
                        <div className="div-block-filter-row-columns">
                            { t("labels.filter_by_published_at") }

                            <BooksYearPickerFilter resetEM={ this.resetEM } 
                                onChange={ (type, year) => this.handleYearChange(type, year) } />
                        </div>

                        <div className="div-block-filter-row-columns">
                            { totalItems } { t("labels.search_results") }
                        </div>
                    </div>

 */    
};

export default withTranslation(["default"])(BooksFilter);