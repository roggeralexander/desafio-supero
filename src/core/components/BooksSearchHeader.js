import React, {
    Component, 
    Fragment 
} from "react";

import { 
    withRouter
} from "react-router-dom";

import { 
    withTranslation
} from "react-i18next";

import AppBar from "@material-ui/core/AppBar";

import { 
    Subject
} from "rxjs";

import _ from "lodash";

import Autocomplete from "../../modules/autocomplete";

import BookStoreService from "../services/BookStoreService";

class BooksSearchHeader extends Component {
    static propTypes = {};

    static defaultProps = {
        ref: () => {}
    };

    constructor(props) {
        super(props);

        this.requestSearchEM = new Subject();
        this.resetEM = new Subject();
        this.selectedIdEM = new Subject();
    };

    componentDidMount() {
        this.subscribeToRequestSearchEM(); 
        this.subscribeToResetEM(); 
        this.subscribeToSelectedIdEM();
    };

    componentWillUnmount() {
        this.requestSearchEM.complete(); 
        this.requestSearchEM = null; 

        this.resetEM.complete(); 
        this.resetEM = null; 

        this.selectedIdEM.complete(); 
        this.selectedIdEM = null; 
    };

    handleAutocompleteReset = () => {
        console.log("Handle autocomplete reset");
    };

    subscribeToRequestSearchEM = () => {
        this.requestSearchEM.subscribe(query => {
            if(_.isNil(query) || query.trim().length === 0) {
                return ;
            }

            const params = new URLSearchParams();
            params.append("q", query);

            const url = `/?${ params }`;
            this.goToRoute(url)
        });
    };

    subscribeToResetEM = () => {

    };

    subscribeToSelectedIdEM = () => {
        this.selectedIdEM.subscribe(id => {
            const url = `/book/${ id }`;
            this.goToRoute(url);
        });
    };

    goToRoute = route => {
        const { history } = this.props;

        setImmediate(() => {
            history.push(route);
            window.location.reload();
        });
    };

    render() {
        const { t } = this.props;

        return(
            <Fragment>
                <AppBar style={{ alignItems: "center", padding: "10px 20px", backgroundColor: "transparent" }}>
                    <div className="div-center-content">
                        <div className="company-logo"></div>

                        <div className="div-search-form">
                            <Autocomplete requestSearchEM={ this.requestSearchEM } 
                                resetEM={ this.resetEM } 
                                selectedIdEM={ this.selectedIdEM }
                                placeholder={ t("labels.search_books_by") } 
                                cb={ BookStoreService.getSuggestions } 
                                onClear={ () => this.handleAutocompleteReset() } />
                        </div>
                    </div>
                </AppBar>
            </Fragment>
        );
    };
};

export default withRouter(
    withTranslation(["default"])(BooksSearchHeader)
);