import React, {
    Component, 
    Fragment
} from "react"; 

import PropTypes from "prop-types";

import { 
    withTranslation
} from "react-i18next";

import AppBar from "@material-ui/core/AppBar"; 
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs"; 
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

import moment from "moment";
import _ from "lodash";

import DefaultImage from "../../modules/image/components/DefaultImage";

import "../styles/books.scss";

class BookDetails extends Component {
    static propTypes = {
        data: PropTypes.any.isRequired
    };

    static defaultProps = {
        ref: () => { }
    };

    constructor(props) {
        super(props); 

        this.state = {
            currentTab: 0
        };
    };

    getISBN = industryIdentifiers => {
        if(_.isNil(industryIdentifiers)) {
            return null; 
        }

        const isbn = industryIdentifiers.find(item => item["type"] === "ISBN_13")["identifier"];
        return isbn
    };

    handleTabChange = ($event, value) => this.setState({
        currentTab: value
    });

    render() {
        const { t, data } = this.props;
        const { volumeInfo } = data;
        const { currentTab } = this.state;

        return(
            <Fragment>
                <div className="div-book-desc" style={{height: "300px" }}>
                    <DefaultImage src={ volumeInfo["imageLinks"]["large"] } 
                        bgSize="contain" 
                        minHeight={ 300 }
                        onLoading={ isLoading => { } }
                        onSuccess={ () => { } } 
                        onError={ () => {} } />    
                </div>
                <div className="div-book-desc">
                    <h1>
                        { volumeInfo["title"] }
                    </h1>
                    { !_.isNil(volumeInfo["subtitle"]) ? (
                        <h3>
                            { volumeInfo["subtitle"] }
                        </h3>
                    ) : null }
                </div>

                        <AppBar>
                            <Tabs value={ currentTab } 
                                onChange={ ($event, value) => this.handleTabChange($event, value) }>
                                <Tab label={ t("labels.description") } />
                                <Tab label={ t("labels.characteristics") } />
                            </Tabs> 
                        </AppBar>

                        { currentTab === 0 && (
                            <Paper>
                                <Typography component="div" style={{ padding: "10px 0px"}}>
                                    { volumeInfo["description"] }
                                </Typography>
                            </Paper>
                        )} 

                        { currentTab === 1 && (
                                <table style={{ margin: "10px 0px"}}>
                                    <tbody>
                                        { !_.isNil(volumeInfo["industryIdentifiers"]) ? (
                                            <tr>
                                                <td>
                                                    ISBN
                                                </td>
                                                <td>
                                                    { this.getISBN(volumeInfo["industryIdentifiers"]) }
                                                </td>
                                            </tr>
                                        ) : null }

                                        { !_.isNil(volumeInfo["authors"]) ? (
                                            <tr>
                                                <td>
                                                    { t("labels.authors") }
                                                </td>
                                                <td>
                                                    { volumeInfo["authors"].map(item => (
                                                        <div key={ item }>{ item }</div>
                                                    ))}
                                                </td>
                                            </tr>
                                        ) : null }

                                        { !_.isNil(volumeInfo["categories"]) ? (
                                            <tr>
                                                <td>
                                                    { t("labels.categories") }
                                                </td>
                                                <td>
                                                    { volumeInfo["categories"].map(item => (
                                                        <div key={ item }>{ item }</div>
                                                    ))}
                                                </td>
                                            </tr>
                                        ) : null }

                                        { !_.isNil(volumeInfo["language"]) ? (
                                            <tr>
                                                <td>
                                                    { t("labels.language") } 
                                                </td>
                                                <td>
                                                    { volumeInfo["language"] }
                                                </td>
                                            </tr>                                        
                                        ) : null }

                                        { !_.isNil(volumeInfo["publisher"]) ? (
                                            <tr>
                                                <td>
                                                    { t("labels.publisher") } 
                                                </td>
                                                <td>
                                                    { volumeInfo["publisher"] }
                                                </td>
                                            </tr>
                                        ) : null }

                                        { !_.isNil(volumeInfo["publishedDate"]) ? (
                                            <tr>
                                                <td>
                                                    { t("labels.publication_year") }
                                                </td>
                                                <td>
                                                    { moment(volumeInfo["publishedDate"]).format("YYYY") }
                                                </td>
                                            </tr>
                                        ) : null }

                                        { !_.isNil(volumeInfo["pageCount"]) ? (
                                            <tr>
                                                <td>
                                                    { t("labels.page_count") }
                                                </td>
                                                <td>
                                                    { volumeInfo["pageCount"] }
                                                </td>
                                            </tr>
                                        ) : null }
                                    </tbody>
                                </table>
                            )
                        }
            </Fragment>
        );
    };
};

export default withTranslation(["default"])(BookDetails);