import {
    Observable, 
    of
} from "rxjs";

import { 
    catchError,
    switchMap
} from "rxjs/operators";

import _ from "lodash";

import { 
    GOOGLE_API_KEY, 
    GOOGLE_BOOKS_API
} from "../configs";

class BookStoreService {
    getHeaders = () => {
        const headers = new Headers();
        headers.set("Content-Type", "application/json");
        return headers;
    };

    getParams = (query, pageNumber, pageSize) => {
        let params = new URLSearchParams();
        params.append("q", query); 
        params.append("printType", "books");
        params.append("maxResults", pageSize);
        params.append("startIndex", pageNumber === 0 ? pageNumber : (pageNumber * pageSize - 1));
        params.append("key", GOOGLE_API_KEY); 
        return params;
    };

    getBasicParams = () => {
        let params = new URLSearchParams();
        params.append("key", GOOGLE_API_KEY); 
        return params;
    };

    searchBy = (query, pageNumber = 0, pageSize = 10) => new Observable(observer => {
        const url = `${ GOOGLE_BOOKS_API }?${ this.getParams(query, pageNumber, pageSize) }`;

        fetch(url, { method: "GET", headers: this.getHeaders() })
            .then(res => {
                if(!res.ok || res.status !== 200) {
                    return Promise.reject("Not allowed to request the api");
                } 

                return res.json();
            })
            .then(data => {
                const { items, totalItems } = data;

                observer.next({
                    count: totalItems, 
                    list: items
                });

                observer.complete();
            })
            .catch(err => {
                observer.error(err);
                observer.complete();
            });
    });

    searchById = id => new Observable(observer => {
        const url = `${ GOOGLE_BOOKS_API }/${ id }?${ this.getBasicParams() }`;

        fetch(url, { method: "GET", headers: this.getHeaders() })
            .then(res => {
                if(!res.ok || res.status !== 200) {
                    return Promise.reject("Not allowed to request the api");
                } 

                return res.json();
            })
            .then(data => {
                observer.next(data);
                observer.complete();
            })
            .catch(err => {
                observer.error(err);
                observer.complete();
            });
    });

    getSuggestions = query => this.searchBy(query, 0, 5).pipe(
        switchMap(obj => {
            const { count, list } = obj;

            return of({
                count: count, 
                list: list.map(item => this.createSuggestionItem(item))
            });
        }), 
        catchError(err => {
            console.log("Error carregando o query:", err);
            return of([]);
        })
    );

    createSuggestionItem = item => {
        const { id, volumeInfo } = item;
        const { title, subtitle, imageLinks } = volumeInfo; 

        let obj = {};
        obj["id"] = id;
        obj["title"] = title;

        if(!_.isNil(subtitle)) {
            obj["subtitle"] = subtitle;
        }

        if(!_.isNil(imageLinks) && !_.isNil(imageLinks["thumbnail"])) {
            obj["thumb"] = imageLinks["thumbnail"];
        }

        return obj;
    };
};

const instance = new BookStoreService(); 

export default instance;