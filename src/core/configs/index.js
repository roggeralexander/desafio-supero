export const GOOGLE_BOOKS_API = process.env.REACT_APP_GOOGLE_BOOKS_API;

export const GOOGLE_API_KEY = process.env.REACT_APP_GOOGLE_API_KEY;