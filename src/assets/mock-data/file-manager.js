export const MOCK_FILE_CONFIGS = {
    "image": {
        "mimeTypes": ["image/jpeg", "image/png", "image/x-ms-bmp", "image/gif"], 
        "extensions": [".jpg", ".jpeg", ".png", ".gif"], 
        "maxSizeInBytes": 10485760, 
        "sizeNumber": 10, 
        "sizeUnit": "MB"
    }
};

export const MOCK_FILE_TYPES_ALLOWED = ["image"];