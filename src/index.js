import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';

import MomentUtils from "@date-io/moment";

import {     
    MuiPickersUtilsProvider
} from "@material-ui/pickers";

import {
    I18nextProvider
} from "react-i18next";

import i18n from "./translations/i18n";

import {
    Router
} from "react-router-dom";

import {
    createBrowserHistory
} from "history";

const rootHtmlElm = document.getElementById("root");

ReactDOM.render(
    <Router history={ createBrowserHistory() }>
        <I18nextProvider i18n={ i18n }>
            <MuiPickersUtilsProvider utils={ MomentUtils }>
                <App />
            </MuiPickersUtilsProvider>
        </I18nextProvider> 
    </Router>,
    rootHtmlElm
);

serviceWorker.unregister();
