import React, {
    Component
} from "react";

import PropTypes from "prop-types";

// Styles 
import "../styles/paginator.scss";

export default class PageButton extends Component {

    static propTypes = {
        label: PropTypes.string.isRequired, 
        value: PropTypes.number.isRequired, 
        isCurrent: PropTypes.bool.isRequired, 
        isFirst: PropTypes.bool.isRequired, 
        isLast: PropTypes.bool.isRequired, 
        onClick: PropTypes.func
    };

    static defaultProps = {
        isCurrent: false, 
        isFirst: false, 
        isLast: false, 
        ref: () => {}
    };

    getStyleClasses = () => {
        const { isCurrent, isFirst, isLast } = this.props;

        return [
            "btn-page", 
            isCurrent ? "active" : "", 
            isFirst ? "is-first" : "", 
            isLast ? "is-last" : ""
        ].join(" ");
    };

    handleClick = $event => {
        const { value, isCurrent } = this.props;

        if(isCurrent) {
            return ; 
        }

        this.props.onClick(value);
    };

    getHtmlBtn = () => {
        const { label } = this.props;

        return (
            <span type="button" 
                className = { this.getStyleClasses() }
                onClick = { $event => this.handleClick($event) } >
                { label }
            </span>
        );
    };

    render() {
        return (
            this.getHtmlBtn()
        );
    };
};