import React, {
    Component
} from "react";

import PropTypes from "prop-types";

import { 
    withTranslation
} from "react-i18next";

import uuid from "uuid";

// Styles 
import "../styles/paginator.scss";

// Keycodes
const ENTER = 13;
const ESCAPE = 27;

class PageInputGoTo extends Component {
    
    static propTypes = {
        totalPages: PropTypes.number.isRequired, 
        onChange: PropTypes.func
    };

    static defaultProps = {
        ref: () => {}
    };

    constructor(props) {
        super(props);

        this.id = uuid.v4();
        this.inputHtml = null;

        this.state = {
            goTo: ""
        };
    };

    componentDidMount() {
        this.inputHtml = document.getElementById(this.id);
    };

    componentWillUnmount() {
        this.inputHtml = null;
    };

    processPage = page => {
        const { totalPages } = this.props;

        let pageToSelect = page;

        if(page <= 1) {
            pageToSelect = 1;
        } else if (page >= totalPages) {
            pageToSelect = totalPages;
        } 

        this.props.onChange(pageToSelect);

        this.resetGoTo();
    };

    handleBlur = $event => {
        if(!$event || !$event.target) {
            return ;
        }

        const { value } = $event.target;

        if(value === undefined || value === null) {
            return ;
        }

        if(value.trim().length) {
            this.processPage(parseInt(value));
        }
    };

    handleChange = $event => {
        const { value } = $event.target;

        if(value === undefined || value === null) {
            return ;
        }

        this.setState({ goTo: value });
    };

    handleKeyDown = $event => {
        const keyCode = $event.keyCode || $event.which;
        const { value } = $event.target;

        if([ESCAPE, ENTER].indexOf(keyCode) === -1 || value === undefined || value === null) { 
            return ;
        }

        if(keyCode === ENTER && value.trim().length) {
            this.processPage(parseInt(value));
            return ;
        }

        this.resetGoTo();
    };

    resetGoTo = () => {
        this.setState({
            goTo: ""
        }, () => {
            if(!this.inputHtml) {
                return ;
            }

            this.inputHtml.blur();
        });
    };

    render() {
        const { t, totalPages } = this.props;
        const { goTo } = this.state;

        return (
            <div className={ "row-paginator" }>
                <span className={ "paginator-label page-range-label" }>
                    { t("labels.paginator_goTo") }: 
                </span>

                <input type="number" 
                    id={ this.id }
                    min="1" 
                    max={ totalPages }  
                    value= { goTo } 
                    onChange={ $event => this.handleChange($event) }
                    onBlur={ $event => this.handleBlur($event) } 
                    onKeyDown={ $event => this.handleKeyDown($event) } />
            </div>
        );
    };
};

export default withTranslation(
    "default"
)(PageInputGoTo);