import React, {
    Component, 
    Fragment
} from "react";

import PropTypes from "prop-types";

import {
    withTranslation
} from "react-i18next";

// Constants 
import {
    PAGE_SIZE, 
    PAGE_SIZE_OPTIONS
} from "../constants";

// Styles 
import "../styles/paginator.scss";

class PageSizeSelector extends Component {

    static propTypes = {
        pageSize: PropTypes.number, 
        pageSizeOptions: PropTypes.array, 
        onChange: PropTypes.func
    };

    static defaultProps = {
        pageSize: PAGE_SIZE, 
        pageSizeOptions: PAGE_SIZE_OPTIONS, 
        ref: () => {}
    };

    getHtmlOptions = () => {
        const { pageSizeOptions } = this.props;

        return pageSizeOptions.map(option => (
            <option key={ option }
                    value={ option }>
                    { option }
            </option>
        ));
    };

    render() {
        const { pageSize, t } = this.props;

        return (
            <Fragment>
                <span className="paginator-label page-range-label">
                    { t("labels.paginator_perPage") }: 
                </span>
                <select className="page-size-selector"
                    value={ pageSize }
                    onChange={ this.props.onChange }>
                    { this.getHtmlOptions() }
                </select>
            </Fragment>
        );
    };
};

export default withTranslation(
    "default"
)(PageSizeSelector);