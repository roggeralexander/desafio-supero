import React, {
    Component, 
    Fragment
} from "react";

import PropTypes from "prop-types";

import {
    withTranslation
} from "react-i18next";

// Constants 
import {
    PAGE_SIZE_OPTIONS
} from "../constants";

// Styles 
import "../styles/paginator.scss";

class PageRangeLabel extends Component {

    static propTypes = {
        pageNumber: PropTypes.number.isRequired, 
        pageSize: PropTypes.number.isRequired, 
        totalItems: PropTypes.number.isRequired
    };

    static defaultProps = {
        pageSize: PAGE_SIZE_OPTIONS, 
        ref: () => {}
    };

    render() {
        const { pageNumber, pageSize, totalItems, t } = this.props;
        const start = (pageNumber - 1) * pageSize + 1;
        const possibleEnd = pageNumber * pageSize;
        const end = possibleEnd >= totalItems ? totalItems : possibleEnd ;
        
        return (
            <Fragment>
                <span className="paginator-label page-range-label">
                    { `${ start } - ${ end } ${ t("labels.of") } ${ totalItems }` }
                </span>
            </Fragment>
        );
    };
};

export default withTranslation(
    "default"
)(PageRangeLabel);