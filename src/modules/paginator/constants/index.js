// Constante para especificar o tamanho de cada página
export const PAGE_SIZE = 50;

// Constante para especificar as opções de tamanho
export const PAGE_SIZE_OPTIONS = [
    10, 
    25, 
    50, 
    100
];