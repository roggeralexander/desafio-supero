import React, {
    Component, 
    Fragment
} from "react";

import PropTypes from "prop-types";

import {
    withTranslation
} from "react-i18next";

import { 
    Observable, 
    of, 
    Subject
} from "rxjs";

import { 
    skip,
    switchMap
} from "rxjs/operators";

import {
    PAGE_SIZE, 
    PAGE_SIZE_OPTIONS
} from "./constants";

import PageButton from "./components/PageButton";
import PageInputGoTo from "./components/PageInputGoTo";
import PageRangeLabel from "./components/PageRangeLabel";
import PageSizeSelector from "./components/PageSizeSelector";

import "./styles/paginator.scss";

import { 
    createPagesObjs
} from "./utils";

class Paginator extends Component {

    static propTypes = {
        totalItems: PropTypes.number.isRequired, 
        pageNumber: PropTypes.number.isRequired, 
        pageSize: PropTypes.number, 
        pageSizeOptions: PropTypes.array, 
        showNumberBtns: PropTypes.bool,
        showFirstLastBtns: PropTypes.bool, 
        showPageGoTo: PropTypes.bool, 
        onChange: PropTypes.func
    };

    static defaultProps = {
        pageSize: PAGE_SIZE, 
        pageSizeOptions: PAGE_SIZE_OPTIONS, 
        showPageGoTo: false,  
        ref: null
    };

    constructor(props) {
        super(props);

        this._subs = [];

        this._worker = new Subject();

        this.pageSize = this.props["pageSize"];

        this.pageNumber = this.props["pageNumber"];

        this.pageSizeOptions = this.props["pageSizeOptions"];

        this.pagesCtrls = [];

        this._subs.push(
            this._worker.pipe(
                switchMap(() => this.updateDisplayedPageSizeOptions()), 
                switchMap(() => this.updateBtnsForPages()), 
                switchMap(() => this.updateStateForPageSizeChanges()), 
                skip(1)
            ).subscribe(
                () => this.props.onChange(this.state)
            )
        );

        this.state = {
            totalItems: this.props["totalItems"],
            pageNumber: this.props["pageNumber"], 
            pageSize: this.props["pageSize"], 
            pageSizeOptions: PAGE_SIZE_OPTIONS
        };
    };

    componentDidMount() {
        this._worker.next();
    };

    componentWillUnmount() {
        this._subs.forEach(sub => sub.unsubscribe());
        this._subs = null; 

        this._worker.complete(); 
        this._worker = null; 

        this.pageSize = null; 
        this.pageNumber = null; 
        this.pageSizeOptions = null; 
        this.pagesCtrls = null; 
    };

    getTotalPages = () => {
        const { totalItems } = this.props;

        return Math.ceil(
            totalItems / this.pageSize
        );
    };

    updateDisplayedPageSizeOptions = () => {
        if(this.pageSize === undefined || !this.pageSize) {
            this.pageSize = PAGE_SIZE_OPTIONS.length ? PAGE_SIZE_OPTIONS[0] : PAGE_SIZE;
        }

        let displayedPageSizeOptions = Array.from(this.pageSizeOptions ? this.pageSizeOptions : PAGE_SIZE_OPTIONS);

        if(displayedPageSizeOptions.indexOf(this.pageSize) === -1) {
            displayedPageSizeOptions.push(this.pageSize);
        } 

        const { totalItems } = this.props;

        const allAreGreatherThanTotalItems = displayedPageSizeOptions.every(a => a > totalItems); 

        if(!allAreGreatherThanTotalItems) {
            displayedPageSizeOptions = displayedPageSizeOptions.filter(a => a <= totalItems);
            displayedPageSizeOptions.push(totalItems);
            displayedPageSizeOptions = Array.from(new Set(displayedPageSizeOptions));
        }

        displayedPageSizeOptions.sort((a, b) => a - b);

        this.pageSizeOptions = displayedPageSizeOptions;

        return of(undefined);
    };

    updateBtnsForPages = () => {
        const options = {
            totalPages: this.getTotalPages(), 
            maxBtns: 7, 
            ellipses: "..."
        };
        
        return createPagesObjs(this.pageNumber, options).pipe(
            switchMap(pages => this.createPagesCtrls(pages))
        );
    };

    updateStateForPageSizeChanges = () => new Observable(observer => {
        this.setState({
            pageSize: this.pageSize, 
            pageSizeOptions: this.pageSizeOptions, 
            pageNumber: this.pageNumber
        }, 
        () => {
            observer.next(); 
            observer.complete();
        })
    });

    createPagesCtrls = pages => {
        this.pagesCtrls = [];

        if(this.isAllowToAppendNumberButtons()) {
            this.pagesCtrls = Array.from(pages);
        }

        this.addExtraPagesCtrls();

        return of(undefined);
    };

    isAllowToAppendNumberButtons = () => {
        const { showNumberBtns } = this.props;

        if(showNumberBtns !== undefined && showNumberBtns !== null) {
            return showNumberBtns;
        }

        return true;
    };

    isAllowToAppendFirstLastNavigationButtons = () => {
        const { showFirstLastBtns } = this.props;

        if(showFirstLastBtns !== undefined && showFirstLastBtns !== null) {
            return showFirstLastBtns;
        }

        return true;
    };

    addPageCtrlForFirstPage = () => {
        if(!this.isAllowToAppendFirstLastNavigationButtons || !this.hasPage("previous")) {
            return ;
        }

        this.pagesCtrls.unshift(
            this.getPageCtrlObj("<<", 1)
        );
    };

    addPageCtrlForLastPage = () => {
        if(!this.isAllowToAppendFirstLastNavigationButtons || !this.hasPage("next")) {
            return ;
        }

        this.pagesCtrls.push(
            this.getPageCtrlObj(">>", this.getTotalPages())
        );
    };
    
    addPageCtrlForPreviousPage = () => {
        if(this.pageNumber <= 1 || !this.hasPage("previous")) {
            return ;
        }

        this.pagesCtrls.unshift(
            this.getPageCtrlObj("<", this.pageNumber - 1)
        );
    };

    addPageCtrlForNextPage = () => {
        if(this.pageNumber >= this.getTotalPages() || !this.hasPage("next")) {
            return ;
        }

        this.pagesCtrls.push(
            this.getPageCtrlObj(">", this.pageNumber + 1)
        );
    };

    addExtraPagesCtrls = () => {
        this.addPageCtrlForPreviousPage();
        this.addPageCtrlForFirstPage();
        this.addPageCtrlForNextPage();
        this.addPageCtrlForLastPage();
    };

    getPageCtrlObj = (label, value) => Object.assign(
        {}, 
        { label: label, value: value }
    ); 

    calculatePageNumberFromItemStartsAt = newPageSize => {
        const previousStart = (this.pageNumber - 1) * this.pageSize + 1;
        this.pageSize = newPageSize;
        this.pageNumber = Math.ceil(previousStart /this.pageSize);
    };
    
    handlePageSizeChange = $event => {
        const pageSize = parseInt($event.target["value"]);

        if(this.pageSize === pageSize) {
            return ;
        }

        this.calculatePageNumberFromItemStartsAt(pageSize);
        this._worker.next();
    }; 

    handlePageClick = pageNumber => {
        if(this.pageNumber === pageNumber) {
            return ; 
        }

        this.pageNumber = pageNumber;
        this._worker.next();
    };

    hasPage = action => {
        const totalPages = this.getTotalPages();

        switch(action) {
            case "previous": 
                return this.pageNumber > 1 && totalPages > 1;
            default: 
                return (totalPages > 1) && (this.pageNumber < totalPages);
        }
    };

    getHtmlPageButtons = () => {
        const { pageNumber, pageSize } = this.state;
        const { totalItems } = this.props;

        if(pageSize === totalItems) {
            return null; 
        }

        const buttons = this.pagesCtrls.map((pageCtrl, index) => (
            <PageButton
                key={ index }
                { ...pageCtrl }
                isCurrent = { pageNumber === pageCtrl["value"] } 
                isFirst = { index === 0 } 
                isLast = { index === this.pagesCtrls.length - 1 } 
                onClick = { $event => this.handlePageClick($event) } />
        ));

        return buttons.length ? 
            (
                <div className="row-paginator">
                    { buttons }
                </div>
            ) : null;            
    };

    render() {
        const { pageNumber, pageSize, pageSizeOptions } = this.state;
        const { totalItems, showPageGoTo } = this.props;

        return (
            <Fragment>
                <div className="paginator-container">
                    <div className="row-paginator">
                        <PageSizeSelector 
                            pageSize = { pageSize }
                            pageSizeOptions = { pageSizeOptions }
                            onChange = { $event => this.handlePageSizeChange($event) } />

                        <PageRangeLabel 
                            pageNumber = { pageNumber }
                            pageSize = { pageSize } 
                            totalItems = { totalItems } />                    
                    </div>

                    { this.getHtmlPageButtons() }

                    { 
                        showPageGoTo && totalItems !== pageSize ? 
                        (
                            <PageInputGoTo 
                                totalPages={ this.getTotalPages() }
                                onChange={ $event => this.handlePageClick($event) } />                    
                        ) : null 
                    }
                </div>
            </Fragment>
        );
    };
};

export default withTranslation(
    "default"
)(Paginator);