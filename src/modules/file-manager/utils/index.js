import fileSize from "file-size";

import _ from "lodash";

export const getEntry = ($event, index) => {
    let entry = null; 

    if($event.dataTransfer.items) {
        if($event.dataTransfer.items[index].webkitGetAsEntry) {
            entry = $event.dataTransfer.items[index].webkitGetAsEntry();
        }
    } else if($event.dataTransfer.files) {
        if($event.dataTransfer.files[index].webkitGetAsEntry) {
            entry = $event.dataTransfer.files[index].webkitGetAsEntry();
        } 
    }

    return entry;
};

export const createFileFromEntry = (entry, cb) => entry.file(cb);

export const traverseFilesTree = (entry, path, cbWhenIsFile) => {
    if(entry.isFile) {
        createFileFromEntry(entry, cbWhenIsFile);
    } else {
        path += "/";

        const dirReader = entry.createReader();
        let entries = [];

        const readEntries = () => {
            dirReader.readEntries(res => {
                if(!res.length) {
                    for(let i = 0; i < entries.length; i++) {
                        traverseFilesTree(entries[i], path + entries[i].name, cbWhenIsFile);
                    }
                } else {
                    entries = entries.concat(res);
                    readEntries();
                }
            });
        };

        readEntries();
    }
};

export const getFileExtension = filename => filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);

export const getHumanizedFileSize = totalBytes => fileSize(totalBytes).human("si");

export const isFileExtensionAllowed = (file, allowedExtensions) => {
    if(_.isNil(allowedExtensions) || !allowedExtensions.length || allowedExtensions.indexOf("*") !== -1) {
        return true;
    }

    const extension = `.${ getFileExtension(file.name) }`;

    return allowedExtensions.some(ext => ext.trim().toLowerCase() === extension);
};

export const getFileTypeFromConfigs = (file, configs = undefined) => {
    if(_.isNil(configs) || !Object.keys(configs).length) {
        return null; 
    }

    const mimeType = file.type;
    const fileType = Object.keys(configs).find(key => configs[key]["mimeTypes"].indexOf(mimeType) !== -1);
    return fileType;
};

export const isFileSizeOk = (file, configs = undefined) => {
    if(_.isNil(configs) || !Object.keys(configs).length) {
        return true; 
    }

    const fileType = getFileTypeFromConfigs(file, configs);

    if(_.isNil(fileType)) {
        return false;
    }

    return file.size <= configs[fileType]["maxSizeInBytes"];
};

export const getFileExtensionsFromConfigs = (configs = undefined, fileTypes = undefined) => {
    if(_.isNil(configs) || !Object.keys(configs).length) {
        return ["*"];
    }

    return getFileExtensionsFromConfigsAndFileTypes(configs, fileTypes);
};

export const getFileExtensionsFromConfigsAndFileTypes = (configs, fileTypes = undefined) => {
    if(!_.isNil(fileTypes) && fileTypes.indexOf("*") !== -1) {
        return ["*"];
    }

    let extensions = [];

    Object.keys(configs).forEach(key => {
        if(!_.isNil(fileTypes) && fileTypes.indexOf(key) !== -1 && !_.isNil(configs[key]["extensions"]) && configs[key]["extensions"].length > 0) {
            extensions = extensions.concat(configs[key]["extensions"]);
        } else if(!_.isNil(configs[key]["extensions"]) && configs[key]["extensions"].length > 0) {
            extensions = extensions.concat(configs[key]["extensions"]);
        }
    });

    return Array.from(new Set(extensions));
};