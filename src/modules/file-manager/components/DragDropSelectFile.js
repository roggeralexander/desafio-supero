import React, {
    Component, 
    Fragment 
} from "react";

import PropTypes from "prop-types";

import { 
    withTranslation
} from "react-i18next";

import { 
    createFileFromEntry, 
    getEntry, 
    getFileExtension, 
    getFileExtensionsFromConfigs,
    getFileTypeFromConfigs,
    getHumanizedFileSize, 
    isFileExtensionAllowed, 
    isFileSizeOk, 
    traverseFilesTree
} from "../utils";

import "../styles/file-manager.scss";

class DragDropSelectFile extends Component {
    static propTypes = {
        configs: PropTypes.any, 
        fileTypes: PropTypes.array, 
        isMultipleSelect: PropTypes.bool, 
        onSuccess: PropTypes.func, 
        onError: PropTypes.func
    };

    static defaultProps = {
        ref: () => { }, 
        configs: null, 
        fileTypes: ["*"], 
        isMultipleSelect: false
    };

    constructor(props) {
        super(props);

        this.fileInputRef = React.createRef();

        this.state = {
            allowedExtensions: ["*"]
        };
    };

    componentDidMount() {
        this.setAllowedExtensions();
    };

    componentWillUnmount() {
        this.fileInputRef = null; 
    };

    setAllowedExtensions = () => {
        const { configs, fileTypes } = this.props;
        const allowedExtensions = getFileExtensionsFromConfigs(configs, fileTypes);

        this.setState({
            allowedExtensions: allowedExtensions
        });
    };

    handleFileChange = $event => {
        const source = $event.target.files[0];
        this.checkFile(source);
    };

    handleRequestToClickFileInput = ($event) => {
        const fileInputHTMLElm = this.fileInputRef.current;

        if(!fileInputHTMLElm) {
            return ;
        }

        fileInputHTMLElm.click();
    };

    handleDragOver = $event => {
        this.stopAndPrevent($event);
    };

    handleDragLeave = $event => {
        this.stopAndPrevent($event);
    };

    handleDrop = $event => {
        $event.dataTransfer.dropEffect = "copy";
        this.dragDropFiles($event);
        this.stopAndPrevent($event);
    };

    stopAndPrevent = $event => {
        $event.stopPropagation(); 
        $event.preventDefault();
    };

    checkFile = file => {
        const { configs } = this.props;
        const { allowedExtensions } = this.state;
        const isExtensionOk = isFileExtensionAllowed(file, allowedExtensions);
        const isSizeOk = isFileSizeOk(file, configs);

        if(!isExtensionOk || !isSizeOk) {
            return ;
        }

        this.sendFileObjData(file);
    };

    sendFileObjData = file => {
        const { configs } = this.props;

        const objInfo = {
            type: getFileTypeFromConfigs(file, configs), 
            file: file, 
            mimeType: file.type, 
            extension: getFileExtension(file.name), 
            size: file.size, 
            humanizedSize: getHumanizedFileSize(file.size)
        };

        setImmediate(() => this.props.onSuccess(objInfo));
    };

    dragDropFiles = $event => {
        const totalEntries = $event.dataTransfer.items ? 
            $event.dataTransfer.items.length : 
            $event.dataTransfer.files.length;

        for(let i = 0; i < totalEntries; i++) {
            let entry = getEntry($event, i); 

            if(!entry) {
                continue;
            }

            if(entry.isFile) {
                createFileFromEntry(entry, this.checkFile);
            } else if(entry.isDirectory) {
                traverseFilesTree(entry, entry["name"], this.checkFile);
            }
        }
    };

    render() {
        const { t } = this.props;
        const { allowedExtensions } = this.state;

        return(
            <Fragment>
                <div className = "drag-drop-select-container" 
                     draggable = "true" 
                     onClick = { $event => this.handleRequestToClickFileInput($event) } 
                     onDragOver = { $event => this.handleDragOver($event) } 
                     onDragLeave = { $event => this.handleDragLeave($event) } 
                     onDrop = { $event => this.handleDrop($event) }>
                    <span>
                        { t("labels.drag_drop_select_files") }
                    </span>

                    <input  type="file" 
                            ref={ this.fileInputRef } 
                            accept={ allowedExtensions.join(",") }
                            className="drag-drop-select-file-input"
                            onChange={ $event => this.handleFileChange($event) } />
                </div>
            </Fragment>
        );
    };
};

export default withTranslation(
    "default"
)(DragDropSelectFile);