export const ALERT_OPTIONS = {
    title: "", 
    showCancelButton: true, 
    showCloseButton: true, 
    showConfirmButton: true, 
    confirmButtonText: "", 
    cancelButtonText: ""
};
