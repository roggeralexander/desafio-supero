import { 
    from, 
    of, 
    throwError
} from "rxjs";

import { 
    switchMap
} from "rxjs/operators";

import Swal from "sweetalert2";

import _ from "lodash";

import {
    ALERT_OPTIONS
} from "../constants";

class AlertService {
    getAlertOptions = (title, confirmButtonText, cancelButtonText) => Object.assign(
        {}, ALERT_OPTIONS, { title: title, confirmButtonText: confirmButtonText, cancelButtonText: cancelButtonText }
    );

    openAlertToConfirmRemove = alertOptions => from(Swal.fire(alertOptions)).pipe(
        switchMap(result => {
            if(_.isNil(result["value"])) {
                return throwError(new Error(""));
            }

            return of(undefined);
        })
    );
};

const instance = new AlertService(); 

export default instance;