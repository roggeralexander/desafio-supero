import React, {
    Component, 
    Fragment 
} from "react";

import PropTypes from "prop-types";

import { 
    of
} from "rxjs";

import { 
    switchMap
} from "rxjs/operators";

import _ from "lodash";

import ImgLoader from "../classes/ImgLoader";

import "../styles/image.scss";

export default class DefaultImage extends Component {
    static propTypes = {
        src: PropTypes.any.isRequired, 
        bgSize: PropTypes.oneOf(['cover', 'contain']), 
        minHeight: PropTypes.number,
        onLoading: PropTypes.func, 
        onSuccess: PropTypes.func, 
        onError: PropTypes.func
    };

    static defaultProps = {
        ref: () => {}, 
        src: null, 
        bgSize: 'cover', 
        minHeight: 150
    };

    constructor(props) {
        super(props);

        this.imgContainerRef = React.createRef();
        this.imgLoader = new ImgLoader();
        this.src = null;

        this.state = {
            isLoading:  false
        };  

        this.subscribeForImgLoader();
    };

    componentDidMount() {
        const { src } = this.props;

        if(_.isNil(src) || (typeof src === "string" && !src.trim().length)) {
            return ;
        }

        this.src = src instanceof File ? URL.createObjectURL(src) : src;

        this.imgLoader.setSrc(this.src);
    };

    componentWillUnmount() {
        this.imgLoader.destroy();
        this.imgLoader = null; 

        this.imgContainerRef = null;
    };

    subscribeForImgLoader = () => {
        this.imgLoader.isLoading.subscribe(isLoading => this.setState({
            isLoading: isLoading
        }, () => this.props.onLoading(isLoading)));

        this.imgLoader.onSuccess.pipe(
            switchMap(() => this.renderImg())
        ).subscribe(() => this.props.onSuccess(undefined));

        this.imgLoader.onError.subscribe(err => this.props.onError(err));
    };

    containerHasHeight = () => {
        const imgContainer = this.imgContainerRef.current;
        const { height } = imgContainer.getBoundingClientRect();
        return height > 0;
    };

    renderAsBG = () => {
        this.addStylesOnImgContainer();        
        return of(undefined);
    };

    addStylesOnImgContainer = () => {
        const { bgSize, minHeight } = this.props;
        const imgContainer = this.imgContainerRef.current;
        imgContainer.classList.add("full-bg-img");
        imgContainer.classList.add(bgSize);
        imgContainer.style.setProperty("background-image", `url(${ this.src })`, "important");

        if(!this.containerHasHeight()) {
            imgContainer.style.setProperty("min-height", `${ minHeight }px`, "important");
        }
    };

    resetStylesOnImgContainer = () => {
        const imgContainer = this.imgContainerRef.current;
        imgContainer.classList.remove("full-bg-img");
        imgContainer.classList.remove("contain");
        imgContainer.classList.remove("cover");
        imgContainer.style.setProperty("background-image", "none", "important");
        imgContainer.style.setProperty("min-height", "inherit", "important");
    };

    renderAsHTMLElement = () => {
        const imgContainer = this.imgContainerRef.current;
        this.resetStylesOnImgContainer();
        
        while(imgContainer.firstChild) {
            imgContainer.removeChild(imgContainer.firstChild);
        }

        imgContainer.appendChild(this.imgLoader.htmlImg);

        return of(undefined);
    };

    renderImg = () => {
        const { bgSize } = this.props;
        return bgSize ? this.renderAsBG() : this.renderAsHTMLElement();
    };

    render() {
        return(
            <Fragment>
                <div ref={ this.imgContainerRef } 
                     className="img-container">
                </div>
            </Fragment>
        );
    };
};