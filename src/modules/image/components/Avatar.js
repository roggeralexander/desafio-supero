import React, {
    Component, 
    Fragment
} from "react";

import PropTypes from "prop-types";

import {
    Avatar as MaterialAvatar
} from "@material-ui/core";

import _ from "lodash";

import ImgLoader from "../classes/ImgLoader";

import AvatarActions from "./AvatarActions";

import {
    getFirstLetterEachWords
} from "../../utils/string";

import "../styles/image.scss";

export default class Avatar extends Component {
    static propTypes = {
        src: PropTypes.any, 
        sizeType: PropTypes.oneOf(["s", "m", "l", "xl"]), 
        allowActions: PropTypes.bool, 
        displayName: PropTypes.string, 
        onApplyAction: PropTypes.func
    };

    static defaultProps = {
        ref: () => { }, 
        src: null, 
        sizeType: "s", 
        allowActions: false, 
        displayName: "Avatar Generator"
    };

    constructor(props) {
        super(props); 

        this.avatarContainerRef = React.createRef();
        this.displayName = this.props["displayName"];
        this.subs = [];
        this.imgLoader = new ImgLoader();

        this.state = {
            type: null, 
            source: null, 
            allowToShow: false
        };
    };

    componentDidMount() { 
        this.initialize();
    };

    componentWillUnmount() {
        this.imgLoader.destroy();
        this.imgLoader = null;

        this.avatarContainerRef = null;

        this.subs.forEach(sub => {
            sub.unsubscribe(); 
            sub = null;
        });

        this.subs = null; 
    };

    initialize = () => {
        this.subscribeToImgLoader();
        this.setAvatarSource();
    };

    updateAvatarSourceState = (type, source, allowToShow) => this.setState({
        type: type, 
        source: source, 
        allowToShow: allowToShow
    });

    subscribeToImgLoader = () => {
        this.imgLoader.onSuccess.subscribe(() => this.updateAvatarSourceState(
            "image", this.imgLoader.src, true)
        );

        this.imgLoader.onError.subscribe(() => this.updateAvatarSourceState(
            "letter", this.displayName, true)
        );
    };

    setAvatarSource = () => {
        const { src } = this.props;

        this.displayName = getFirstLetterEachWords(this.displayName, true);

        if(!_.isNil(src)) {
            this.imgLoader.setSrc(src);
            return ;
        }

        this.updateAvatarSourceState("letter", this.displayName, true);
    };

    renderAvatarImage = () => {
        const { sizeType } = this.props;
        const { source } = this.state;

        return (
            <MaterialAvatar src={ source } className={ sizeType } />
        );
    };

    renderAvatarLetter = () => {
        const { source } = this.state;
        const { sizeType } = this.props;

        return (
            <MaterialAvatar className={ `orangeAvatar ${ sizeType }` }>
                { source }
            </MaterialAvatar>
        );
    };

    renderAvatarByType = () => {
        const { type } = this.state;

        switch(type) {
            case "letter": 
                return this.renderAvatarLetter(); 
            case "image": 
                return this.renderAvatarImage() ; 
            default: 
                return null;
        }
    };

    renderActions = () => {
        const { allowActions } = this.props;

        if(!allowActions) {
            return null;
        }

        return (
            <AvatarActions onApplyAction={ $event => this.props.onApplyAction($event) } />
        );
    };

    render() {
        const { allowToShow } = this.state;
        const { sizeType } = this.props;

        return(
            <Fragment>
                { allowToShow ? (
                    <div className={ sizeType } 
                        ref={ this.avatarContainerRef } 
                        style={{ overflow: "hidden", borderRadius: "50%", position: "relative" }}>
                        { this.renderAvatarByType() }
                        { this.renderActions () }
                    </div>
                ) : null}
            </Fragment>
        );
    };
}