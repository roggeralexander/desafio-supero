import React, {
    Component, 
    Fragment
} from "react"; 

import PropTypes from "prop-types";

import { 
    withTranslation
} from "react-i18next";

import { 
    fromEvent
} from "rxjs";

import IconButton from "@material-ui/core/IconButton";
import PhotoCameraIcon from "@material-ui/icons/PhotoCamera";

import Menu from "@material-ui/core/Menu"; 
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";

import { 
    animated, 
    config, 
    Spring
} from "react-spring/renderprops";  

import "../styles/image.scss";

import { 
    DEFAULT_SPRING_CONFIGS
} from "../constants";

class AvatarActions extends Component {
    static propTypes = {
        onApplyAction: PropTypes.func
    };

    static defaultProps = {
        ref: () => { }
    };
    
    constructor(props) {
        super(props);

        this.subs = [];
        this.dropdownTriggerRef = React.createRef();

        this.state = {
            dropdownOpened: false, 
            showActions: false
        };
    };

    componentDidMount() {
        this.subscribeToClickedOutside();
    };

    componentWillUnmount() {
        this.dropdownTriggerRef = null; 

        this.subs.forEach(sub => {
            sub.unsubscribe(); 
            sub = null; 
        });

        this.subs = null; 
    };

    subscribeToClickedOutside = () => {
        this.subs.push(
            fromEvent(window, "click").subscribe(this.handleClickedOnWindowScope), 
            fromEvent(window, "touchstart").subscribe(this.handleClickedOnWindowScope)
        );
    };

    isElementInside = (source, elmRef) => {
        const htmlElm = elmRef.current;
        return htmlElm.contains(source);
    };

    handleClickedOnWindowScope = $event => {
        const source = $event.target;
        const { dropdownOpened } = this.state;

        if(!this.isElementInside(source, this.dropdownTriggerRef)) {
            if(!dropdownOpened) {
                return ;
            }

            this.setState({
                showActions: false
            });
        }

        this.toggleOpenDropdownMenu();
    };

    toggleOpenDropdownMenu = () => {
        const { dropdownOpened } = this.state;
        !dropdownOpened ? this.openDropdownMenu() : this.closeDropdownMenu(); 

        this.setState({
            dropdownOpened: !dropdownOpened
        });
    };

    openDropdownMenu = () => {
        const triggerHTMLElm = this.dropdownTriggerRef.current;
        triggerHTMLElm.classList.add("icon-trigger-active");
    };

    closeDropdownMenu = () => {
        const triggerHTMLElm = this.dropdownTriggerRef.current;
        triggerHTMLElm.classList.remove("icon-trigger-active");
    };

    handleMenuItemClick = $event => this.props.onApplyAction($event);

    handleShowActions = $event => this.setState({
        showActions: $event
    });

    renderMenuDropdown = () => {
        const { t } = this.props;
        const { dropdownOpened } = this.state;
        const triggerHTMLElm = this.dropdownTriggerRef.current;

        return (
            <Menu elevation={ 0 } 
                getContentAnchorEl={ null }
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }} 
                transformOrigin={{ vertical: 'top', horizontal: 'center' }}  
                variant="menu" 
                anchorEl={ triggerHTMLElm } 
                open={ dropdownOpened }>
                <MenuItem onClick={ () => this.handleMenuItemClick("add") }>
                    <ListItemText primary={ t("labels.menu_item_add_avatar") } />
                </MenuItem>
                <MenuItem onClick={ () => this.handleMenuItemClick("remove") }>
                    <ListItemText primary={ t("labels.menu_item_remove_avatar") } />
                </MenuItem>
            </Menu>
        );
    };

    render() {
        const { showActions } = this.state;

        return (
            <Fragment>
                <div className="avatar-actions" 
                    onMouseEnter={ () => this.handleShowActions(true) } 
                    onMouseLeave={ () => this.handleShowActions(false) }>
                    <Spring native 
                        from={{ opacity: 0, display: "none" }} 
                        to={{ opacity: showActions ? 1 : 0, display: showActions ? "flex" : "none" }} 
                        config={{ ...config, DEFAULT_SPRING_CONFIGS }}>
                        {
                            style => (
                                <animated.div style={ style } className="icon-trigger-container">
                                    <IconButton ref={ this.dropdownTriggerRef } 
                                        className="avatar-actions-icon-trigger-btn">
                                        <PhotoCameraIcon />
                                    </IconButton>

                                    { this.renderMenuDropdown() }                               
                                </animated.div>
                            )
                        }
                    </Spring>
                </div>
            </Fragment>
        );
    };
}

export default withTranslation(["default"])(AvatarActions);