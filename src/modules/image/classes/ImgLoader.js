import { 
    fromEvent, 
    Observable, 
    of, 
    Subject
} from "rxjs";

import { 
    catchError, 
    switchMap
} from "rxjs/operators";

export default class ImgLoader {
    constructor(src) {
        this.isLoading = new Subject();
        this.onSuccess = new Subject();
        this.onError = new Subject();
        this.worker = new Subject();
        this.htmlImg = null;
        this.src = null; 

        this.subscribeToWorker();
    };

    destroy = () => {
        this.destroyHtmlImg();
        
        this.isLoading.complete();
        this.isLoading = null;

        this.onSuccess.complete();
        this.onSuccess = null;

        this.onError.complete();
        this.onError = null;

        this.worker.complete();
        this.worker = null;
    };

    setSrc = src => {
        this.src = src;
        setImmediate(() => this.worker.next());
    };

    destroyHtmlImg = () => {
        if(!this.htmlImg) {
            return ;
        }

        this.htmlImg = null;
    };

    loadImg = () => new Observable(observer => {
        this.destroyHtmlImg();
        this.isLoading.next(true);

        this.htmlImg = new Image();

        const onNext = () => {
            observer.next();
            observer.complete();
        };

        const onError = err => {
            observer.error(err);
            observer.complete();
        };

        fromEvent(this.htmlImg, "load").subscribe(onNext, onError);
        
        this.htmlImg.src = this.src;        
    });

    subscribeToWorker = () => {
        this.worker.pipe(
            switchMap(() => this.loadImg()), 
            switchMap(() => {
                this.isLoading.next(false);
                this.onSuccess.next();
                return of(undefined);
            }), 
            catchError(err => {
                this.isLoading.next(false);
                this.onError.next(err);
                return of(undefined);
            })
        ).subscribe(() => { });
    };
};