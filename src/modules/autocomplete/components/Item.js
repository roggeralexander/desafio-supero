import React, {
    Component, 
    Fragment
} from "react"; 

import PropTypes from "prop-types";

import MenuItem from "@material-ui/core/MenuItem";

import _ from "lodash";

import ItemMiniDesc from "./ItemMiniDesc";
import ItemWithThumb from "./ItemWithThumb";

export default class Item extends Component {
    static propTypes = {
        data: PropTypes.any.isRequired
    };

    static defaultProps = {
        ref: () => { }
    };

    getItemRendered = () => {
        const { data } = this.props;
        const { thumb } = data;
        const ItemComponent = _.isNil(thumb) ? ItemMiniDesc : ItemWithThumb;

        return (
            <ItemComponent { ...this.props } />
        );
    };

    render() {
        return(
            <Fragment>
                <MenuItem component="div">
                    { this.getItemRendered() }
                </MenuItem>
            </Fragment>
        );
    };
};