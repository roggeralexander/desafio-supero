import React, {
    Component, 
    Fragment
} from "react"; 

import PropTypes from "prop-types";

import "../styles/autocomplete.scss";

export default class ItemMiniDesc extends Component {
    static propTypes = {
        data: PropTypes.any.isRequired
    };

    static defaultProps = {
        ref: () => { }
    };

    render() {
        const { data } = this.props;
        const { title, subtitle } = data;

        return (
            <Fragment>
                <div className="mini-desc">
                    <div className="title">
                        { title }
                    </div>
                    <div className="subtitle">
                        { subtitle }
                    </div>
                </div>
            </Fragment>
        );
    };

};