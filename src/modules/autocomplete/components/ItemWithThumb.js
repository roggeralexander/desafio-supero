import React, {
    Component, 
    Fragment
} from "react"; 

import PropTypes from "prop-types";

import ItemMiniDesc from "./ItemMiniDesc";

import DefaultImage from "../../image/components/DefaultImage";

import "../styles/autocomplete.scss";

export default class ItemWithThumb extends Component {
    static propTypes = {
        data: PropTypes.any.isRequired
    };

    static defaultProps = {
        ref: () => { }
    };

    render() {
        const { data } = this.props;
        const { thumb } = data;

        return (
            <Fragment>
                <div className="autocomplete-item">
                    <div className="thumb">
                        <DefaultImage src={ thumb } 
                            bgSize="cover"
                            minHeight={ 40 }
                            onLoading={ isLoading => { } }
                            onSuccess={ () => { } } 
                            onError={ () => {} } />
                    </div>

                    <div className="mini-desc-with-img">
                        <ItemMiniDesc data={ data } />
                    </div>
                </div>
            </Fragment>
        );
    };
};