import React, {
    Component, 
    Fragment
} from "react";

import PropTypes from "prop-types";

import { 
    of, 
    Subject
} from "rxjs";

import { 
    debounceTime, 
    distinctUntilChanged, 
    switchMap
} from "rxjs/operators";

import _ from "lodash";

import Autosuggest from "react-autosuggest";

import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import ResetIcon from "@material-ui/icons/Clear";
import SearchIcon from "@material-ui/icons/Search";

import Item from "./components/Item";

import {
    stripStringSync
} from "../utils/string";

import { 
    DEFAULT_DEBOUNCE_FILTER_TIME, 
    MIN_CHARS_TO_APPLY_AUTOCOMPLETE
} from "./constants";

import "./styles/autocomplete.scss";

export default class Autocomplete extends Component {
    static propTypes = {
        requestSearchEM: PropTypes.any.isRequired, 
        selectedIdEM: PropTypes.any.isRequired, 
        placeholder: PropTypes.string, 
        resetEM: PropTypes.any, 
        delayTime: PropTypes.number, 
        minChars: PropTypes.number,
        onClear: PropTypes.func, 
        cb: PropTypes.func
    };

    static defaultProps = {
        ref: () => { }, 
        placeholder: "", 
        resetEM: null, 
        delayTime: DEFAULT_DEBOUNCE_FILTER_TIME, 
        minChars: MIN_CHARS_TO_APPLY_AUTOCOMPLETE, 
        cb: query => of([])
    };

    constructor(props) {
        super(props);

        this.worker = new Subject();

        this.state = {
            query: "",
            isLoading: false, 
            showResetBtn: false, 
            allowRequestSearch: false, 
            suggestions: [], 
            totalItems: 0
        };
    };

    componentDidMount() {
        this.subscribeToWorker();
        this.subscribeToResetEM();
    };

    componentWillUnmount() {
        this.worker.complete(); 
        this.worker = null;
    };

    subscribeToWorker = () => {
        const { delayTime, cb } = this.props;

        const onNext = result => this.setState({ 
            isLoading: false, 
            showResetBtn: true, 
            suggestions: result["list"], 
            totalItems: result["count"], 
            allowRequestSearch: true
        });

        this.worker.pipe(
            distinctUntilChanged(), 
            debounceTime(delayTime), 
            switchMap(query => {
                this.setState({ isLoading: true, showResetBtn: false, allowRequestSearch: false });
                return cb(query);
            })
        ).subscribe(onNext);
    };

    subscribeToResetEM = () => {
        const { resetEM } = this.props;

        if(_.isNil(resetEM)) {
            return ;
        }

        resetEM.subscribe(() => this.handleResetState());
    };  

    handleSuggestionsChange = ($event, { newValue } ) => {
        this.setState({
            query: newValue
        });
    };

    getStrippedQuery = value => {
        let strippedQuery = stripStringSync(value, { removeSpecialChars: true });
        strippedQuery = _.isNil(strippedQuery) ? "" : strippedQuery;
        return strippedQuery;
    };

    handleSuggestionsFetchRequested = ({ value }) => {
        this.worker.next(this.getStrippedQuery(value));
    };

    handleResetState = () => this.setState({
        query: "", 
        isLoading: false, 
        showResetBtn: false, 
        allowRequestSearch: false, 
        suggestions: [], 
        totalItems: 0
    }, () => this.props.onClear(undefined));

    handleSuggestionsClearRequested = () => this.setState({
        isLoading: false, 
        suggestions: [], 
        totalItems: 0
    });

    shouldRenderSuggestions = value => {
        const { minChars } = this.props;
        const strippedQuery = this.getStrippedQuery(value);
        return strippedQuery.trim().length >= minChars;
    };

    renderSuggestion = suggestion => (
        <Item data={ suggestion } />
    );

    getSuggestionValue = suggestion => {
        const { selectedIdEM } = this.props;
        const { title, id } = suggestion;
        selectedIdEM.next(id);
        return title;
    };

    handleRequestSearch = () => {
        const { requestSearchEM } = this.props;
        const { query } = this.state;

        requestSearchEM.next(this.getStrippedQuery(query));
    };

    render() {
        const { query, suggestions, isLoading, showResetBtn, allowRequestSearch } = this.state;
        const { placeholder } = this.props;

        const inputProps = {
            placeholder: placeholder, 
            value: query, 
            onChange: this.handleSuggestionsChange
        };

        return(
            <Fragment>
                <div className="autocomplete-container">
                    <Autosuggest 
                        suggestions={ suggestions } 
                        onSuggestionsFetchRequested={ this.handleSuggestionsFetchRequested } 
                        onSuggestionsClearRequested={ this.handleSuggestionsClearRequested }
                        getSuggestionValue={ suggestion => this.getSuggestionValue(suggestion) } 
                        renderSuggestion={ suggestion => this.renderSuggestion(suggestion) }
                        shouldRenderSuggestions={ this.shouldRenderSuggestions }
                        inputProps = { inputProps } />

                    <div className="autocomplete-actions">
                        { isLoading ? (
                                <div className="loader-spinner">
                                    <CircularProgress color="primary" />
                                </div>
                            ) : null 
                        }

                        { showResetBtn ? (
                                <div className="autocomplete-reset-btn"
                                    onClick={ () => this.handleResetState() }>
                                    <ResetIcon />
                                </div>
                            ) : null
                        }
                    </div>
                </div>
                <Button variant="contained" 
                    disabled={ !allowRequestSearch }
                    onClick={ () => this.handleRequestSearch() } >
                    <SearchIcon />
                </Button>
            </Fragment>
        );
    };
};