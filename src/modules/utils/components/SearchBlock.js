import React, {
    Component, 
    Fragment
} from "react"; 

import PropTypes from "prop-types";

import { 
    Subject
} from "rxjs";

import { 
    debounceTime, 
    distinctUntilChanged
} from "rxjs/operators";

import _ from "lodash";

import Button from "@material-ui/core/Button";
import ResetIcon from "@material-ui/icons/Clear";
import SearchIcon from "@material-ui/icons/Search";

import { 
    DEFAULT_DEBOUNCE_FILTER_TIME, 
    MIN_CHARS_TO_APPLY_AUTOCOMPLETE
} from "../constants";

import {
    stripStringSync
} from "../string";

import "../styles/utils.scss";

export default class SearchBlock extends Component {
    static propTypes = {
        placeholder: PropTypes.string,
        resetEM: PropTypes.any, 
        delayTime: PropTypes.number, 
        minChars: PropTypes.number, 
        onRequestSearch: PropTypes.func     
    };

    static defaultProps = {
        ref: () => { }, 
        placeholder: "", 
        delayTime: DEFAULT_DEBOUNCE_FILTER_TIME, 
        minChars: MIN_CHARS_TO_APPLY_AUTOCOMPLETE
    };

    constructor(props) {
        super(props);

        this.worker = new Subject();

        this.state = {
            query: "", 
            showResetBtn: false, 
            allowSearchBtn: false
        };
    };

    componentDidMount() {
        this.subscribeToWorker();
        this.subscribeToResetEM();
    };

    componentWillUnmount() {
        this.worker.complete(); 
        this.worker = null;
    };

    subscribeToWorker = () => {
        const { delayTime } = this.props;

        this.worker.pipe(
            distinctUntilChanged(), 
            debounceTime(delayTime)
        ).subscribe(query => this.setState({
            allowSearchBtn: query.trim().length >= minChars
        }));
    };

    subscribeToResetEM = () => {
        const { resetEM } = this.props;

        if(_.isNil(resetEM)) {
            return ;
        }

        resetEM.subscribe(() => this.handleResetState());        
    };

    getStrippedQuery = value => {
        let strippedQuery = stripStringSync(value, { removeSpecialChars: true });
        strippedQuery = _.isNil(strippedQuery) ? "" : strippedQuery;
        return strippedQuery;
    };

    handleOnChange = $event => {
        const { value } = $event.target;
        const query = _.isNil(value) ? "" : this.getStrippedQuery(value);
        const showResetBtn = query.length > 0;

        this.setState({
            query: query, showResetBtn: showResetBtn
        }, () => this.worker.next(query));
    };

    handleRequestSearch = () => {
        const { query } = this.state;
        this.props.onRequestSearch(query);
    };

    handleResetState = () => this.setState({
        query: "", 
        showResetBtn: false, 
        allowSearchBtn: false
    });

    render() {
        const { placeholder } = this.props;
        const { query, showResetBtn, allowSearchBtn } = this.state;

        return(
            <Fragment>
                <div className="search-block-container">
                    <input type="text" 
                        placeholder={ placeholder }
                        value={ query }
                        onChange={ $event => this.handleOnChange($event) } />

                    <div className="search-actions">
                        { 
                            showResetBtn ? (
                                <div class="search-reset-btn" 
                                    onClick={ () => this.handleResetState() }>
                                        <ResetIcon />
                                    </div>
                            ) : null 
                        }
                    </div>

                    <div className="search-actions">
                        <Button variant="contained" 
                            disabled={ !allowSearchBtn }
                            onClick={ () => this.handleRequestSearch() } >
                            <SearchIcon />
                        </Button>
                    </div>
                </div>
            </Fragment>
        );
    };
};