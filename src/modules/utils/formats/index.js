import formatString from "format-string-by-pattern";

import { 
    PHONE_MASK
} from "../masks";

export const formatPhoneNumber = value => {
    const onlyNumbers = value.replace(/[^\d]/g, "");
    return formatString(PHONE_MASK, onlyNumbers);
};