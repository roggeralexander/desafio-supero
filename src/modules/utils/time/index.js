import moment from "moment";

import _ from "lodash";

export const getFormattedDuration = (duration, format = "full") => {
    let strFormat = "";
    let hours = 0, minutes = 0, seconds = 0;
  
    hours = Math.floor(duration / 3600);
  
    if(hours > 0) {
        if(hours > 9 ) {
            strFormat += `${ hours }:`;
        } else if(hours > 0 && hours <= 9) {
            strFormat += `0${ hours }:`;
        }
  
        minutes = Math.floor((duration - hours * 3600) / 60);
  
        seconds = Math.ceil((duration - hours * 3600) - minutes * 60);
    } else {
        minutes = Math.floor(duration / 60);
  
        if(minutes > 0) {
            seconds = Math.ceil(duration % 60);            
        } else {
            seconds = Math.ceil(duration);
        }
  
        if(format !== undefined && format === "full") {
            strFormat += "00:";
        }
    }
  
    if(minutes > 9) {
        strFormat += `${ minutes }:`;
    } else {
        strFormat += `0${ minutes }:`;
    }
  
    if(seconds > 9) {
        strFormat += seconds;
    } else {
        strFormat += `0${ seconds }`;
    }
  
    return strFormat;    
};

export const getMonths = () => moment.months();

export const getTotalDaysInMonth = (year, month) => moment(`${ year }-${ month }`, "YYYY-MM").daysInMonth();

export const getValueAsDate = value => {
    switch(value.constructor.name) {
        case "Date":
            return value;
        default: 
            return moment(value).toDate();
    }
};

export const isDateBetweenAges = (date, minAge = 0, maxAge = 100) => {
    if(_.isNil(date) || minAge < 0 || maxAge <= 0 || minAge >= maxAge) {
        return false;
    }

    const a = moment(date);
    const b = moment();
    const years = Math.abs(a.diff(b, "year"));
    
    return years >= minAge && years <= maxAge;
};

export const getDifferenceBetweenDates = (start, end = Date.now(), unit = "days") => {
    const startMoment = moment(start);
    const endMoment = moment(end);
    const diff = startMoment.diff(endMoment);
    const duration = moment.duration(diff);

    let durationAbs = null;
    let returnHumanize = false;
    let returnFormatted = false;

    switch(unit) {
        case "days": 
            durationAbs = Math.abs(duration.asDays());
    
            if(durationAbs < 1) {
                return null;
            } else if(durationAbs >= 1 && durationAbs < 7) {
                returnHumanize = true;
            } else {
                returnFormatted = true;
            }

            break;
        case "hours": 
            durationAbs = Math.abs(duration.asHours());
    
            if(durationAbs < 1) {
                return null;
            } else if(durationAbs >= 1 && durationAbs < 24) {
                returnHumanize = true;
            } 

            break; 
        case "minutes": 
            durationAbs = Math.abs(duration.asMinutes());
    
            if(durationAbs < 1) {
                return null;
            } else if(durationAbs >= 1 && durationAbs < 60) {
                returnHumanize = true;
            } 

            break;
        case "seconds": 
            returnHumanize = true;
            break;
    }

    if(returnHumanize) {
        return duration.humanize(true);
    }

    if(returnFormatted) {
        return startMoment.format("DD MMM - HH:mm");
    }

    return null;

};

export const isDateBetweenDatesRange = (date, minDate, maxDate) => {
    let isOk = false;

    if(date === undefined || date === null) {
        return false;
    }

    let currentDate = moment(date).toDate().getTime();
    let min = null;
    let max = null;

    if(minDate !== null || maxDate !== null) {
        if(minDate !== null) {
            min = moment(minDate).toDate().getTime();
        }

        if(maxDate !== null) {
            max = moment(maxDate).toDate().getTime();
        }

        if(min === null) {
            isOk = currentDate <= max;
        } else if(max === null) {
            isOk = currentDate >= min;
        } else {
            isOk = currentDate >= min && currentDate <= max;
        }
    }

    return isOk;    
};