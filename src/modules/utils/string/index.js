import { 
    of
} from "rxjs"; 

import _ from "lodash";

import striptags from "striptags"; 

export const stripHtmlSync = (html, allowedTags) => striptags(html, allowedTags).trim();

export const stripHtml = (html, allowedTags) => of(
    stripHtmlSync(html, allowedTags)
);

export const removeAcentSync = str => {
    if(str === undefined || !str || !str.trim().length) {
        return null; 
    }

    let accents = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    let accentsOut = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    let arrayStr = str.split('');

    arrayStr.forEach((letter, index) => {
        let i = accents.indexOf(letter);
  
        if (i !== -1) {
            arrayStr[index] = accentsOut[i];
        }
    });

    return arrayStr.join('');    
};

export const removeAcent = str  => of(
    removeAcentSync(str)
);

export const stripStringSync = (str, options) => {
    let value = removeAcentSync(
        str
    );

    if(_.isNil(value)) {
        return null; 
    }

    const { removeSpecialChars, removeNumberChars } = options;

    if(!_.isNil(removeSpecialChars) && removeSpecialChars) {
        value = value.replace(/[^\w\s]/gi, ' ');
    }

    if(!_.isNil(removeNumberChars) && removeNumberChars) {
        value = value.replace(/\d+/g, "");
    } 

    value = value.replace(/_/g, ""); 
    value = value.replace(/ +/g, " ");

    return value;
};

export const stripString = (str, options) => of(
    stripStringSync(
        str, 
        options
    )
);

export const replaceWithParamsSync = (msg, params) => {
    if(msg === undefined || !msg || !msg.trim().length) {
        return null;
    }

    if(params === undefined || !params || !Object.keys(params).length) {
        return msg;
    }

    let message = msg;

    Object.keys(params).forEach(key => {
        message = message.replace(
            new RegExp(
                `\\$${ key }`, 
                "gi"
            ), 
            params[key]
        );
    });

    return message;
};

export const replaceWithParams = (msg, params) => of(
    replaceWithParamsSync(
        msg, 
        params
    )
);

export const getFirstLetterEachWords = (value, capitalize = false) => {
    let matches = value.match(/\b(\w)/g);
    return capitalize ? matches.join("").toUpperCase() : matches.join("");
};
