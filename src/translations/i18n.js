import detector from "i18next-browser-languagedetector";
import i18n from "i18next";
import backend from "i18next-xhr-backend";
import {
    initReactI18next
} from "react-i18next";

const NAMESPACES = [
    "default"
];

const BACKEND_OPTIONS = {
    loadPath: '/locales/{{lng}}/{{ns}}.json',
    crossDomain: false,
    withCredentials: false, 
};

const CONFIGS = {
    fallbackLng: "en",
    initialLanguage: "en",
    debug: false, 
    interpolation:{
        escapeValue: false
    },
    backend: BACKEND_OPTIONS, 
    ns: NAMESPACES
};

i18n.use(backend)
    .use(detector)
    .use(initReactI18next)
    .init(CONFIGS);

export default i18n;