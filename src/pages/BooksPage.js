import React, {
    Component, 
    Fragment
} from "react";

import { 
    withRouter
} from "react-router-dom";

import {
    withTranslation
} from "react-i18next";

import { 
    Subject
} from "rxjs"; 

import _ from "lodash";

import queryString from "query-string";

import Paginator from "../modules/paginator";

import BookStoreService from "../core/services/BookStoreService";

import BooksTable from "../core/components/BooksTable";

import "../core/styles/books.scss";

class BooksPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            query: "",
            books: [], 
            totalSearchResults: 0, 
            pageNumber: 1, 
            pageSize: 10
        };
    };

    componentDidMount() {
        this.subscribeToActiveRoute();
    };

    componentWillUnmount() {
    };

    subscribeToActiveRoute = () => {
        const qs = queryString.parse(this.props.location.search);

        if(_.isNil(qs)) {
            return ;
        }        

        const { q, pageSize, pageNumber } = qs;

        this.setState(
            {
                query: _.isNil(q) ? "" : q, 
                pageNumber: _.isNil(pageNumber) ? 1 : parseInt(pageNumber), 
                pageSize: _.isNil(pageSize) ? 10 : parseInt(pageSize)
            }, 
            () => this.loadDataPaginated()
        );
    };

    loadDataPaginated = () => {
        const { query, pageNumber, pageSize } = this.state;

        if(_.isNil(query) || query.trim().length === 0) {
            return ;
        }

        const onNext = obj => {
            const { list, count } = obj; 
            this.setState({ books: list, totalSearchResults: count })
        };

        const onError = err => {
            console.log("Error:", err.message);
        };

        BookStoreService.searchBy(query, pageNumber - 1, pageSize).subscribe(onNext, onError);
    };

    handlePaginatorChange = $event => {
        const { pageNumber, pageSize } = $event;

        this.setState({
            pageNumber: pageNumber, 
            pageSize: pageSize
        }, () => this.createRouteToGo() );
    };

    createRouteToGo = () => {
        const { query, pageNumber, pageSize } = this.state;

        if(_.isNil(query)) {
            return ;
        }

        const params = new URLSearchParams(); 
        params.append("q", query);

        if(!_.isNil(pageNumber)) {
            params.append("pageNumber", pageNumber);
        }

        if(!_.isNil(pageSize)) {
            params.append("pageSize", pageSize);
        }

        const url = `/?${ params }`;
        this.goToRoute(url);
    };

    goToRoute = route => {
        const { history } = this.props;

        setImmediate(() => {
            history.push(route); 
            window.location.reload();
        });
    };

    renderPaginator = () => {
        const { totalSearchResults, pageNumber, pageSize } = this.state;
 
        return totalSearchResults ? (
            <div className="div-paginator-container">
                <Paginator totalItems={ totalSearchResults } 
                    pageNumber={ pageNumber }
                    pageSize={ pageSize }
                    onChange={ $event => this.handlePaginatorChange($event) } />
            </div>
        ) : null;
    };

    renderTotalSearchResults = () => {
        const { totalSearchResults } = this.state;
        const { t } = this.props;

        if(_.isNil(totalSearchResults) || !totalSearchResults) {
            return null;
        }

        return (
            <div className="div-row">
                <span></span>
                <span>
                    { totalSearchResults } { t("labels.search_results") }
                </span>
            </div>
        );
    };

    render() {
        const { books } = this.state;

        return(
            <Fragment>
                { this.renderTotalSearchResults() }

                <BooksTable books={ books } />

                { this.renderPaginator() }
            </Fragment>
        );
    };
};

export default withRouter(
    withTranslation("default")(BooksPage)
);