import React, {
    Component, 
    Fragment
} from "react";

import { 
    withRouter
} from "react-router-dom";

import {
    withTranslation
} from "react-i18next";

import { 
    Subject
} from "rxjs"; 

import _ from "lodash";

import BookDetails from "../core/components/BookDetails";

import BookStoreService from "../core/services/BookStoreService";

class BookDetailsPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            book: null
        };
    };

    componentDidMount() {
        const { id } = this.props.match.params;

        if(_.isNil(id)) {
            return ;
        }

        this.getBookInfoById(id);
    };

    getBookInfoById = id => {
        const onNext = data => this.setState({
            book: data
        }, () => console.log("Informações do livro:", data));

        const onError = err => {
            console.log("Error carregando o livro:", err);
        };

        BookStoreService.searchById(id).subscribe(onNext, onError);
    };

    render() {
        const { book } = this.state;
      
        return(
            <Fragment>
                { !_.isNil(book) ? (
                    <BookDetails data={ book } />
                ) : null }
            </Fragment>
        );
    };
};

export default withRouter(
    withTranslation("default")(BookDetailsPage)
);