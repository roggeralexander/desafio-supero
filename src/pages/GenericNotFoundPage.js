import React, {
    Component, 
    Fragment
} from "react";

import { 
    withRouter
} from "react-router-dom";

import {
    withTranslation
} from "react-i18next";

class GenericNotFoundPage extends Component {
    render() {
        const { t } = this.props;

        return(
            <Fragment>
                <p>
                    { t("labels.page_not_found") }<br />
                    { t("labels.advice_not_found") }
                </p>
            </Fragment>
        );
    };
};

export default withRouter(
    withTranslation(["default"])(GenericNotFoundPage)
);