import React, {
    Component, 
    Fragment
} from "react";

import { 
    Route, 
    Switch
} from "react-router-dom";

import BooksSearchHeader from "../core/components/BooksSearchHeader";

import BooksPage from "../pages/BooksPage";
import BookDetailsPage from "../pages/BookDetailsPage"; 
import GenericNotFoundPage from "../pages/GenericNotFoundPage";

export default class AppRouter extends Component {
    render() {
        return(
            <Fragment>
                <div className="div-row">
                    <BooksSearchHeader />
                </div>

                <div className="div-row router-outlet">
                    <div className="div-center-content">
                        <Switch>
                            <Route exact path="/" component={ BooksPage } />
                            <Route path="/book/:id" component={ BookDetailsPage } />
                            <Route path="*" component={ GenericNotFoundPage } />
                        </Switch>
                    </div>
                </div>
            </Fragment>
        );
    };
};