import React, { 
  Component, 
  Fragment, 
  Suspense
} from 'react';

import AppRouter from "./routes/AppRouter";

export default class App extends Component {
  render() {
    return (
      <Fragment>
        <Suspense fallback="loading">
          <AppRouter />
        </Suspense>
      </Fragment>
    );
  };
};

/*
import DragDropSelectFile from "./modules/file-manager/components/DragDropSelectFile";
import DefaultImage from "./modules/image/components/DefaultImage";

import { 
  MOCK_FILE_CONFIGS, 
  MOCK_FILE_TYPES_ALLOWED
} from "./assets/mock-data/file-manager";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fileToPreview: null
    };
  };

  handlePaginatorChange = $event => {
    console.log("Mudanças detectadas no paginator:", $event);
  };

  handleItemsCheckedChange = $event => {
    console.log("Mudança de seleção de items:", $event);
  };

  handleFileObjectData = $event => {
    console.log("Object ready to preview:", $event);

    this.setState({
      fileToPreview: $event["file"]
    }, () => console.log("File to preview:", this.state["fileToPreview"]));
  };

  handleRequestChooseNewFile = () => {
    this.setState({
      fileToPreview: null
    });
  };

  render() {
    const { fileToPreview } = this.state;

    return (
      <Fragment>
        <Suspense fallback="loading">
          { fileToPreview ? 
              (
                <Fragment>
                  <DefaultImage src={ fileToPreview }  
                                minHeight={ 300 } 
                                asBackground={ 'contain' }
                                onLoading = { $event => console.log("Loading the image:", $event) } 
                                onSuccess = { () => console.log("Image successfully loaded") } 
                                onError = { err => console.log("Image loading error:", err.message) } />
                  <button type="button" 
                          onClick={ () => this.handleRequestChooseNewFile() }>
                    Escolher outro arquivo
                  </button>
                </Fragment>
              )
            : (
                <DragDropSelectFile configs={ MOCK_FILE_CONFIGS } 
                                    fileTypes={ MOCK_FILE_TYPES_ALLOWED } 
                                    onSuccess= { $event => this.handleFileObjectData($event) } />
            ) 
          }
        </Suspense>  
      </Fragment>
    );
  };
};
*/